import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

/**
 * Created by amen on 8/10/17.
 */
public class McDonald extends Observable{

    public McDonald() {
        for(int i=0; i < 10; i++){
            Employee e = new Employee();
            addObserver(e); // dodajemy observera
        }
    }

    public void handleOrder(String order){
        setChanged();               // zaznaczamy zmianę
        notifyObservers(order);     // informujemy o zmianie i wiadomości
                                // którą chcemy przekazać do observerów
    }
}
